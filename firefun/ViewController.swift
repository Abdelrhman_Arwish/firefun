//
//  ViewController.swift
//  firefun
//
//  Created by Abdelrhman on 7/13/17.
//  Copyright © 2017 Abdelrhman. All rights reserved.
//

import UIKit
import FirebaseDatabase
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var myTextField: UITextField!
    
    @IBOutlet weak var myTableView: UITableView!
    var handle:DatabaseHandle?
    var myList:[String]=[]
    var ref:DatabaseReference?
    @IBAction func saveBtn(_ sender: UIButton) {
        
        if myTextField.text != ""{
            ref?.child("list").childByAutoId().setValue(myTextField.text)
            myTextField.text = ""
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = myList[indexPath.row]
        return cell
    }
    //hello
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ref = Database.database().reference()
        handle = ref?.child("list").observe(.childAdded, with: { (snapshot) in
            if let item = snapshot.value as? String{
                self.myList.append(item)
                self.myTableView.reloadData()
            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

